typedef struct node {
	char str;
	struct node *prev, *next;
}node;

typedef struct Integer{
	node *p, *q;
}Integer;

void initInteger(Integer *a);
void addDigit(Integer *a, char c);
Integer createIntegerFromString(char *str);
Integer addIntegers(Integer a, Integer b);
Integer substractIntegers(Integer a, Integer b);
void printInteger(Integer a);
void destroyInteger(Integer *a);
void reverse(Integer *a);
int length(Integer a);
