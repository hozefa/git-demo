#include "queue.h"
#include <stdlib.h>

void qinit(queue *q) {
	q -> head = q -> tail = NULL;
}
void enq(queue *q, data d2) {
	node *x, *tmp;
	x = (node *)malloc(sizeof(node));
	x -> d1 = d2;
	if (q -> head == NULL) {
		q -> head = q -> tail = x;
		x -> next = x;
		return;
	}
	tmp = q -> head;
	q -> head = x;
	x -> next = tmp;
	q -> tail -> next = x;
}
void append_to_front(queue *q, data d) {
	node *tmp;
	node *data_node;
	if(q -> head == q -> tail)
		if(q->head == NULL)
			enq(q, d);
	else if(q->head != NULL) {
		data_node = (node *) malloc(sizeof(node));
                data_node->d1 = d;
                data_node->next = q->head;
                q->head = data_node;
                q->tail->next = q->head;
        }
}
data deq(queue *q) {
	data d3;
	node *p, *r;
	if (q -> head == q -> tail) {
		d3 = q -> head -> d1;
		free(q -> head);
		q -> head = q -> tail = NULL;
		return d3;
	}
	p = q -> head;
	while (p -> next != q -> head) {
		r = p;
		p = p -> next;
	}
	d3 = p -> d1;
	q -> tail = r;
	r -> next = q -> head;
	free(p);
	return d3;
}
data peek(queue *q) {
    data d3;
    d3 = deq(q);
    append_to_front(q, d3);
    return d3;
}
int qfull(queue *q) {
	return 0;
}
int qempty(queue *q) {
	return q -> head == NULL;
}
